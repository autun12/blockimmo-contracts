const Web3 = require('web3');

let web3js;
let web3jsInfura;

window.addEventListener('load', () => {
  if (typeof web3 !== 'undefined' && web3) {
    web3js = new Web3(web3.currentProvider);
  } else {
    window.addEventListener('message', ({ data }) => {
      if (data && data.type === 'ETHEREUM_PROVIDER_SUCCESS') {
        web3js = new Web3(window.ethereum);
      }
    });

    window.postMessage({ type: 'ETHEREUM_PROVIDER_REQUEST' }, '*');
  }
});

function getWeb3js () {
  if (typeof web3js === 'undefined' || !web3js || !web3js.currentProvider.isConnected()) {
    if (typeof web3 !== 'undefined' && web3) {
      web3js = new Web3(web3.currentProvider);
      return web3js;
    } else {
      return getWeb3jsInfura();
    }
  } else {
    return web3js;
  }
}

function getWeb3jsInfura () {
  if (typeof web3jsInfura === 'undefined' || !web3jsInfura || !web3jsInfura.currentProvider.connected) {
    web3jsInfura = new Web3(new Web3.providers.WebsocketProvider('wss://mainnet.infura.io/ws'));
  }

  return web3jsInfura;
}

function getWeb3jsLedger () {
  const createLedgerSubprovider = require('@ledgerhq/web3-subprovider').default;
  const ProviderEngine = require('web3-provider-engine');
  const WebsocketSubprovider = require('web3-provider-engine/subproviders/websocket.js');
  const TransportU2F = require('@ledgerhq/hw-transport-u2f').default;

  const engine = new ProviderEngine();
  engine.addProvider(createLedgerSubprovider(() => TransportU2F.create(), { accountsLength: 10, networkId: 3 }));

  engine.addProvider(new WebsocketSubprovider({ rpcUrl: 'wss://mainnet.infura.io/ws' }));
  engine.start();

  return new Web3(engine);
}

function initContract (name, address, infura = false, ledger = false) {
  const { abi } = require(`./build/contracts/${name}.json`);

  const _web3js = ledger ? getWeb3jsLedger() : infura ? getWeb3jsInfura() : getWeb3js();
  return new _web3js.eth.Contract(abi, address);
}

/**
 const web3js = blockimmoContracts.getWeb3js();
 const { toHex } = web3js.utils;

 const abi = '';
 const data = '';

 const contract = new web3js.eth.Contract(abi);
 contract.deploy({ data }).send({ from: '0x7c01EB2f7F98eEf60447BF620136d2dFA9Ee5420', gas: 2000000, gasPrice: toHex(10e9) }).then(console.log);
 */

export { getWeb3js, getWeb3jsInfura, getWeb3jsLedger, initContract };
