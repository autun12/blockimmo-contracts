pragma solidity 0.4.25;

import "openzeppelin-solidity/contracts/ownership/Claimable.sol";
import "openzeppelin-solidity/contracts/access/rbac/RBAC.sol";


/**
 * @title Whitelist
 * @dev A minimal, simple database mapping public addresses (ie users) to their permissions.
 *
 * `TokenizedProperty` references `this` to only allow tokens to be transferred to addresses with necessary permissions.
 * `TokenSale` references `this` to only allow tokens to be purchased by addresses within the necessary permissions.
 *
 * `WhitelistProxy` enables `this` to be easily and reliably upgraded if absolutely necessary.
 * `WhitelistProxy` and `this` are controlled by a centralized entity (blockimmo).
 *  This centralization is required by our legal framework to ensure investors are known and fully-legal.
 */
contract Whitelist is Claimable, RBAC {
  function grantPermission(address _operator, string _permission) public onlyOwner {
    addRole(_operator, _permission);
  }

  function revokePermission(address _operator, string _permission) public onlyOwner {
    removeRole(_operator, _permission);
  }

  function grantPermissionBatch(address[] _operators, string _permission) public onlyOwner {
    for (uint256 i = 0; i < _operators.length; i++) {
      addRole(_operators[i], _permission);
    }
  }

  function revokePermissionBatch(address[] _operators, string _permission) public onlyOwner {
    for (uint256 i = 0; i < _operators.length; i++) {
      removeRole(_operators[i], _permission);
    }
  }
}
