import { ether } from 'openzeppelin-solidity/test/helpers/ether';

const BigNumber = web3.BigNumber;

const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should();

const EVMRevert = require('openzeppelin-solidity/test/helpers/EVMRevert.js');

const LandRegistry = artifacts.require('LandRegistry');
const LandRegistryProxy = artifacts.require('LandRegistryProxy');
const TokenizedProperty = artifacts.require('TokenizedProperty');
const Whitelist = artifacts.require('Whitelist');
const WhitelistProxy = artifacts.require('WhitelistProxy');

const Medianizer = artifacts.require('Medianizer');

contract('TokenizedProperty', ([blockimmo, account1, account2, managementCompany, from]) => {
  const eGrid = 'CH327417066524';
  const grundstuckNumber = '566';

  const value = ether(1);
  const fee = value.div(100);
  const valueAfterFee = value.sub(fee);

  before(async function () {
    this.medianizer = await Medianizer.new({ from: blockimmo });
    this.medianizer.address.should.be.equal('0x0f5ea0a652e851678ebf77b69484bfcd31f9459b');

    this.landRegistryProxy = await LandRegistryProxy.new({ from: blockimmo });
    this.landRegistryProxy.address.should.be.equal('0xec8be1a5630364292e56d01129e8ee8a9578d7d8');
    this.landRegistry = await LandRegistry.new({ from: blockimmo });
    await this.landRegistryProxy.set(this.landRegistry.address, { from: blockimmo });

    (await this.landRegistryProxy.landRegistry.call()).should.be.equal(this.landRegistry.address);

    this.whitelistProxy = await WhitelistProxy.new({ from: blockimmo });
    this.whitelistProxy.address.should.be.equal('0xc4c7497fbe1a886841a195a5d622cd60053c1376');
    this.whitelist = await Whitelist.new({ from: blockimmo });
    await this.whitelistProxy.set(this.whitelist.address);
    (await this.whitelistProxy.whitelist.call()).should.be.equal(this.whitelist.address);

    const authorizedRole = 'authorized';
    await this.whitelist.grantPermissionBatch([blockimmo, account1, account2], authorizedRole);
  });

  beforeEach(async function () {
    this.contract = await TokenizedProperty.new(eGrid, grundstuckNumber, { from });
    this.totalSupply = await this.contract.totalSupply.call();

    await this.landRegistry.tokenizeProperty(eGrid, this.contract.address, { from: blockimmo }).should.be.fulfilled;

    this.numTokens = await this.contract.NUM_TOKENS.call();
    this.decimals = await this.contract.decimals.call();
  });

  afterEach(async function () {
    await this.landRegistry.untokenizeProperty(eGrid, { from: blockimmo }).should.be.fulfilled;
  });

  describe('deposit', () => {
    it('deposits dividends', async function () {
      const blockimmoBalance = web3.eth.getBalance(blockimmo);

      await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value });
      const balance = web3.eth.getBalance(this.contract.address);
      balance.should.be.bignumber.equal(valueAfterFee);

      const profit = web3.eth.getBalance(blockimmo) - blockimmoBalance;
      assert(Math.abs(profit - fee) < 1e16);
    });

    it('no value', async function () {
      try {
        await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value: 0 });
        assert(false);
      } catch (e) {
        // good
      }
    });
  });

  describe('collectOwedDividends', () => {
    it('collect dividends', async function () {
      const blockimmoBalance = web3.eth.getBalance(blockimmo);

      await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value });
      const balance = web3.eth.getBalance(from);

      const { logs } = await this.contract.collectOwedDividends({ from });
      logs.length.should.be.equal(1);
      logs[0].event.should.be.equal('DividendsCollected');
      logs[0].args.collector.should.be.equal(from);
      logs[0].args.amount.should.be.bignumber.equal(valueAfterFee);

      const dividends = web3.eth.getBalance(from) - balance;
      assert(Math.abs(dividends - valueAfterFee) < 1e16);

      const profit = web3.eth.getBalance(blockimmo) - blockimmoBalance;
      assert(Math.abs(profit - fee) < 1e16);

      web3.eth.getBalance(this.contract.address).should.be.bignumber.equal(0);
    });

    it('no dividends', async function () {
      const { logs } = await this.contract.collectOwedDividends({ from });
      logs.length.should.be.equal(1);
      logs[0].event.should.be.equal('DividendsCollected');
      logs[0].args.collector.should.be.equal(from);
      logs[0].args.amount.should.be.bignumber.equal(0);
    });

    it('different payouts', async function () {
      await this.contract.transfer(account1, this.totalSupply / 1e18, { from }).should.be.fulfilled;
      await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value });

      // await this.contract.collectOwedDividends({ from: account1 }).should.be.fulfilled;

      const balance = web3.eth.getBalance(from);
      await this.contract.collectOwedDividends({ from });

      const dividends = web3.eth.getBalance(from) - balance;
      assert(Math.abs(dividends - valueAfterFee) < 1e16);

      await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value });
      const { logs } = await this.contract.collectOwedDividends({ from: account1 });
      logs[0].args.amount.should.be.bignumber.equal(1);
    });

    it('three accounts', async function () {
      await this.contract.transfer(account1, this.totalSupply / 1e17, { from });
      await this.contract.transfer(account2, this.totalSupply / 1e18, { from });
      await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value });

      const { logs } = await this.contract.collectOwedDividends({ from: account1 });
      logs[0].args.amount.should.be.bignumber.equal(9);

      await this.contract.collectOwedDividends({ from: account2 }).should.be.fulfilled;

      const r = await this.contract.collectOwedDividends({ from });
      r.logs[0].args.amount.should.be.bignumber.equal(valueAfterFee.sub(11));

      // web3.eth.getBalance(this.contract.address).should.be.bignumber.equal(0);
    });

    it('round down', async function () {
      await this.contract.transfer(account1, this.totalSupply / 1e19, { from }).should.be.fulfilled;
      await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value });

      await this.contract.collectOwedDividends({ from: account1 }).should.be.fulfilled;

      const { logs } = await this.contract.collectOwedDividends({ from });
      logs[0].args.amount.should.be.bignumber.equal(valueAfterFee.sub(1));

      // web3.eth.getBalance(this.contract.address).should.be.bignumber.equal(0);
    });
  });

  describe('integration tests', () => {
    it('transfer after deposit', async function () {
      await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value });
      await this.contract.transfer(account1, this.totalSupply / 2, { from });

      const balance = web3.eth.getBalance(from);
      const { logs } = await this.contract.collectOwedDividends({ from });
      logs[0].args.amount.should.be.bignumber.equal(valueAfterFee);
      const dividends = web3.eth.getBalance(from) - balance;
      assert(Math.abs(dividends - valueAfterFee) < 1e16);

      this.contract.collectOwedDividends({ from }).should.be.fulfilled;
      web3.eth.getBalance(this.contract.address).should.be.bignumber.equal(0);
    });
  });

  it('accumulate dividends', async function () {
    await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value });
    await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value });

    const balance = web3.eth.getBalance(from);
    await this.contract.collectOwedDividends({ from });
    const dividends = web3.eth.getBalance(from) - balance;
    assert(Math.abs(dividends - (valueAfterFee.mul(2))) < 2e16);
  });

  it('accumulate dividends between transfers', async function () {
    await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value });
    await this.contract.transfer(account1, this.totalSupply / 2, { from });

    await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value });

    const balance = web3.eth.getBalance(from);
    await this.contract.collectOwedDividends({ from });
    const dividends = web3.eth.getBalance(from) - balance;
    assert(Math.abs(dividends - (valueAfterFee.mul(1.5))) < 2e16);

    const { logs } = await this.contract.collectOwedDividends({ from: account1 });
    logs[0].args.amount.should.be.bignumber.equal(valueAfterFee.div(2));
  });

  it('accumulate dividends in small amounts (rounding down)', async function () {
    const blockimmoBalance = web3.eth.getBalance(blockimmo);

    await this.contract.transfer(account1, this.totalSupply / 1e19, { from });

    await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value });
    // this.contract.collectOwedDividends({ from: account1 }).should.be.fulfilled;  // shows losing credits
    await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value: ether(10) });

    const { logs } = await this.contract.collectOwedDividends({ from: account1 });
    logs[0].args.amount.should.be.bignumber.equal(1);

    const profit = web3.eth.getBalance(blockimmo) - blockimmoBalance;
    assert(Math.abs(profit - fee.mul(11)) < 1e16);
  });

  it('deposit and collect dividends when untokenized', async function () {
    await this.landRegistry.untokenizeProperty(eGrid, { from: blockimmo }).should.be.fulfilled;

    await web3.eth.sendTransaction({ from: managementCompany, to: this.contract.address, value });
    const balance = web3.eth.getBalance(from);

    await this.contract.collectOwedDividends({ from });

    const dividends = web3.eth.getBalance(from) - balance;
    assert(Math.abs(dividends - valueAfterFee) < 1e16);

    web3.eth.getBalance(this.contract.address).should.be.bignumber.equal(0);

    await this.landRegistry.tokenizeProperty(eGrid, this.contract.address, { from: blockimmo }).should.be.fulfilled;
  });
});
