import { advanceBlock } from 'openzeppelin-solidity/test/helpers/advanceToBlock';
import { assertRevert } from 'openzeppelin-solidity/test/helpers/assertRevert';
import { ether } from 'openzeppelin-solidity/test/helpers/ether';
import { EVMRevert } from 'openzeppelin-solidity/test/helpers/EVMRevert';
import { increaseTimeTo, duration } from 'openzeppelin-solidity/test/helpers/increaseTime';
import { latestTime } from 'openzeppelin-solidity/test/helpers/latestTime';

const BigNumber = web3.BigNumber;

const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should();

BigNumber.config({ DECIMAL_PLACES: 0, ROUNDING_MODE: 0 });

const LandRegistry = artifacts.require('LandRegistry');
const LandRegistryProxy = artifacts.require('LandRegistryProxy');
const TokenizedProperty = artifacts.require('TokenizedProperty');
const TokenSale = artifacts.require('TokenSale');
const Whitelist = artifacts.require('Whitelist');
const WhitelistProxy = artifacts.require('WhitelistProxy');

const SplitPayment = artifacts.require('SplitPayment');
const Medianizer = artifacts.require('Medianizer');

contract('TokenSale', ([from, wallet, investor, cappedInvestor, unauthorized]) => {
  const eGrid = 'CH327417066524';
  const grundstuckNumber = 'CH-ZG-566';

  const conversionRate = 0x000000000000000000000000000000000000000000000020f39d027901310000;
  const cappedLimit = (new BigNumber(50000)).mul(1e18).mul(1e18).div(conversionRate);
  const minInvestment = ether(1);

  const rate = 10;
  const goal = ether(100);
  const cap = ether(1000);
  const authorizedRole = 'authorized';
  const uncappedRole = 'uncapped';

  before(async function () {
    this.medianizer = await Medianizer.new({ from });
    this.medianizer.address.should.be.equal('0x0f5ea0a652e851678ebf77b69484bfcd31f9459b');

    this.landRegistryProxy = await LandRegistryProxy.new({ from });
    this.landRegistryProxy.address.should.be.equal('0xec8be1a5630364292e56d01129e8ee8a9578d7d8');
    this.landRegistry = await LandRegistry.new({ from });
    await this.landRegistryProxy.set(this.landRegistry.address);
    (await this.landRegistryProxy.landRegistry.call()).should.be.equal(this.landRegistry.address);

    this.whitelistProxy = await WhitelistProxy.new({ from });
    this.whitelistProxy.address.should.be.equal('0xc4c7497fbe1a886841a195a5d622cd60053c1376');
    this.whitelist = await Whitelist.new({ from });
    await this.whitelistProxy.set(this.whitelist.address);
    (await this.whitelistProxy.whitelist.call()).should.be.equal(this.whitelist.address);

    this.whitelist.grantPermissionBatch([from, wallet, investor, cappedInvestor], authorizedRole);
    this.whitelist.grantPermissionBatch([from, wallet, investor], uncappedRole);

    await advanceBlock();
  });

  beforeEach(async function () {
    this.token = await TokenizedProperty.new(eGrid, grundstuckNumber, { from });
    await this.landRegistry.tokenizeProperty(eGrid, this.token.address, { from }).should.be.fulfilled;

    this.totalSupply = await this.token.totalSupply.call();

    this.openingTime = (await latestTime()) + duration.weeks(1);
    this.closingTime = this.openingTime + duration.weeks(1);
    this.afterClosingTime = this.closingTime + duration.seconds(1);

    this.router = await SplitPayment.new([from, wallet], [1, 99]);
    this.tokenSale = await TokenSale.new(this.openingTime, this.closingTime, rate, this.router.address, cap, this.token.address, goal, false);
    await this.whitelist.grantPermission(this.tokenSale.address, authorizedRole);
    await this.token.transfer(this.tokenSale.address, this.totalSupply, { from });
  });

  afterEach(async function () {
    await this.landRegistry.untokenizeProperty(eGrid, { from }).should.be.fulfilled;
  });

  it('should create crowdsale with correct parameters', async function () {
    (await this.tokenSale.openingTime()).should.be.bignumber.equal(this.openingTime);
    (await this.tokenSale.closingTime()).should.be.bignumber.equal(this.closingTime);
    (await this.tokenSale.rate()).should.be.bignumber.equal(new BigNumber(rate));
    (await this.tokenSale.wallet()).should.be.equal(this.router.address);
    (await this.tokenSale.goal()).should.be.bignumber.equal(goal);
    (await this.tokenSale.cap()).should.be.bignumber.equal(cap);
  });

  it('should not accept payments before start', async function () {
    await this.tokenSale.send(ether(1)).should.be.rejectedWith(EVMRevert);
    await this.tokenSale.buyTokens(investor, { from: investor, value: ether(1) }).should.be.rejectedWith(EVMRevert);
  });

  it('should accept payments during the sale', async function () {
    const investmentAmount = ether(1);
    const expectedTokenAmount = rate * investmentAmount;

    await increaseTimeTo(this.openingTime);
    const { logs } = await this.tokenSale.buyTokens(investor, { value: investmentAmount, from: investor }).should.be.fulfilled;
    logs[0].args.purchaser.should.be.equal(investor);
    logs[0].args.beneficiary.should.be.equal(investor);
    logs[0].args.amount.should.be.bignumber.equal(expectedTokenAmount);
  });

  it('should reject payments after end', async function () {
    await increaseTimeTo(this.afterEnd);
    await this.tokenSale.send(ether(1)).should.be.rejectedWith(EVMRevert);
    await this.tokenSale.buyTokens(investor, { value: ether(1), from: investor }).should.be.rejectedWith(EVMRevert);
  });

  it('should reject payments over cap', async function () {
    await increaseTimeTo(this.openingTime);
    await this.tokenSale.send(cap);
    await this.tokenSale.send(1).should.be.rejectedWith(EVMRevert);
  });

  it('should allow finalization and transfer funds to wallet if the goal is reached', async function () {
    await increaseTimeTo(this.openingTime);
    await this.tokenSale.buyTokens(investor, { value: goal, from: investor }).should.be.fulfilled;

    const initialWalletBalance = web3.eth.getBalance(wallet);
    const initialBlockimmoBalance = web3.eth.getBalance(from);
    await increaseTimeTo(this.afterClosingTime);
    await this.tokenSale.finalize({ from, gasPrice: 0 });
    await this.router.claim({ from: wallet, gasPrice: 0 });
    await this.router.claim({ from, gasPrice: 0 });
    const finalWalletBalance = web3.eth.getBalance(wallet);
    const finalBlockimmoBalance = web3.eth.getBalance(from);
    const fee = goal.div(100);
    const profit = goal.minus(fee);
    finalWalletBalance.minus(initialWalletBalance).should.be.bignumber.equal(profit);
    finalBlockimmoBalance.minus(initialBlockimmoBalance).should.be.bignumber.equal(fee);

    await this.tokenSale.withdrawTokens({ from: investor });
    (await this.token.balanceOf(investor)).should.be.bignumber.equal(this.totalSupply);
  });

  it('should allow refunds if the goal is not reached', async function () {
    const balanceBeforeInvestment = web3.eth.getBalance(investor);

    await increaseTimeTo(this.openingTime);
    await this.tokenSale.sendTransaction({ value: goal.div(2), from: investor, gasPrice: 0 });
    await increaseTimeTo(this.afterClosingTime);

    await this.tokenSale.finalize({ from });
    await this.tokenSale.claimRefund({ from: investor, gasPrice: 0 }).should.be.fulfilled;

    const balanceAfterRefund = web3.eth.getBalance(investor);
    balanceBeforeInvestment.should.be.bignumber.equal(balanceAfterRefund);

    (await this.token.balanceOf(from)).should.be.bignumber.equal(this.totalSupply);
  });

  it('should not allow refunds if the goal is reached', async function () {
    await increaseTimeTo(this.openingTime);
    await this.tokenSale.sendTransaction({ value: goal, from: investor, gasPrice: 0 });
    await increaseTimeTo(this.afterClosingTime);

    await this.tokenSale.finalize({ from });
    await assertRevert(this.tokenSale.claimRefund({ from: investor, gasPrice: 0 }));

    (await this.token.balanceOf(wallet)).should.be.bignumber.equal(0);
  });

  it('only accepts buys with proper whitelist', async function () {
    await increaseTimeTo(this.openingTime);

    await this.tokenSale.buyTokens(unauthorized, { value: minInvestment, from: unauthorized }).should.be.rejected;
    await this.tokenSale.buyTokens(cappedInvestor, { value: cappedLimit, from: cappedInvestor }).should.be.fulfilled;
    await this.tokenSale.buyTokens(cappedInvestor, { value: 1e16, from: cappedInvestor }).should.be.rejected;
    await this.tokenSale.buyTokens(investor, { value: cappedLimit.mul(2), from }).should.be.fulfilled;
    await this.tokenSale.buyTokens(investor, { value: cappedLimit.mul(2), unauthorized }).should.be.fulfilled;
  });

  describe('when goal > cap', () => {
    const lowCap = ether(1);
    const highGoal = ether(2);
    it('creation reverts', async function () {
      await (TokenSale.new(this.openingTime, this.closingTime, rate, wallet, lowCap, this.token.address, highGoal, false)).should.be.rejected;
    });
  });

  describe('stable (usd)', () => {
    const usdGoal = new BigNumber(10000);
    const weiGoal = usdGoal.mul(1e18).mul(1e18).div(conversionRate);
    const weiFee = (weiGoal.div(100)).sub(1);
    const weiProfit = weiGoal.sub(weiFee);

    it('goal reached', async function () {
      const token = await TokenizedProperty.new('dumb', 'dumb', { from });
      await this.landRegistry.tokenizeProperty('dumb', token.address, { from }).should.be.fulfilled;
      const totalSupply = await token.totalSupply.call();

      const tokenSale = await TokenSale.new(this.openingTime, this.closingTime, rate, this.router.address, usdGoal.mul(2), token.address, usdGoal.sub(1), true);
      await this.whitelist.grantPermission(tokenSale.address, authorizedRole);
      await token.transfer(tokenSale.address, totalSupply, { from });

      await increaseTimeTo(this.openingTime);
      await tokenSale.buyTokens(investor, { value: weiGoal, from: investor }).should.be.fulfilled;

      const beforeFinalization = web3.eth.getBalance(wallet);
      await increaseTimeTo(this.afterClosingTime);
      await tokenSale.finalize({ from, gasPrice: 0 });
      await this.router.claim({ from, gasPrice: 0 });
      await this.router.claim({ from: wallet, gasPrice: 0 });
      const afterFinalization = web3.eth.getBalance(wallet);

      assert(Math.abs((afterFinalization - beforeFinalization) - weiProfit) <= 1e8);

      await tokenSale.withdrawTokens({ from: investor });
      (await token.balanceOf(investor)).should.be.bignumber.equal(totalSupply);
    });

    it('goal not reached', async function () {
      const token = await TokenizedProperty.new('dumber', 'dumb', { from });
      await this.landRegistry.tokenizeProperty('dumber', token.address, { from }).should.be.fulfilled;
      const totalSupply = await token.totalSupply.call();

      const tokenSale = await TokenSale.new(this.openingTime, this.closingTime, rate, wallet, usdGoal.mul(2), token.address, usdGoal.add(1), true);
      await this.whitelist.grantPermission(tokenSale.address, authorizedRole);
      await token.transfer(tokenSale.address, totalSupply, { from });

      await increaseTimeTo(this.openingTime);
      await tokenSale.buyTokens(investor, { value: weiGoal, from: investor }).should.be.fulfilled;

      const beforeFinalization = web3.eth.getBalance(wallet);
      await increaseTimeTo(this.afterClosingTime);
      await tokenSale.finalize({ from });
      const afterFinalization = web3.eth.getBalance(wallet);

      afterFinalization.minus(beforeFinalization).should.be.bignumber.equal(0);

      await assertRevert(tokenSale.withdrawTokens({ from: investor }));
      (await token.balanceOf(investor)).should.be.bignumber.equal(0);
    });

    it('only accepts buys with proper whitelist', async function () {
      const token = await TokenizedProperty.new('dumbest', 'dumb', { from });
      await this.landRegistry.tokenizeProperty('dumbest', token.address, { from }).should.be.fulfilled;
      const totalSupply = await token.totalSupply.call();
      const bigUsdGoal = new BigNumber(10000000);
      const tokenSale = await TokenSale.new(this.openingTime, this.closingTime, rate, wallet, bigUsdGoal.mul(2), token.address, bigUsdGoal, true);
      await this.whitelist.grantPermission(tokenSale.address, authorizedRole);
      await token.transfer(tokenSale.address, totalSupply, { from });

      await increaseTimeTo(this.openingTime);

      await tokenSale.buyTokens(unauthorized, { value: 1, from: unauthorized }).should.be.rejected;
      await tokenSale.buyTokens(cappedInvestor, { value: cappedLimit, from: cappedInvestor }).should.be.fulfilled;
      await tokenSale.buyTokens(cappedInvestor, { value: 1e16, from: cappedInvestor }).should.be.rejected;
      await tokenSale.buyTokens(from, { value: cappedLimit.mul(2), from }).should.be.fulfilled;
      await tokenSale.buyTokens(from, { value: cappedLimit.mul(2), unauthorized }).should.be.fulfilled;
    });
  });
});
