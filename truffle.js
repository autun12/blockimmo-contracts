require('dotenv').config();
require('babel-register')({ // https://github.com/OpenZeppelin/openzeppelin-solidity/issues/740
  ignore: /node_modules\/(?!openzeppelin-solidity\/test\/helpers)/,
});
require('babel-polyfill');

const HDWalletProvider = require('truffle-hdwallet-provider');

module.exports = {
  solc: {
    optimizer: {
      enabled: true,
      runs: 200,
    },
  },

  networks: {
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*', // eslint-disable-line camelcase
    },
    coverage: {
      host: 'localhost',
      network_id: '*',
      port: 8555,
      gas: 0xfffffffffff,
      gasPrice: 0x01,
    },
    ganache: {
      host: 'localhost',
      port: 8545,
      network_id: '*',
    },
    ropsten: { // SEE: https://github.com/trufflesuite/truffle/issues/660
      provider: () => new HDWalletProvider(process.env.MNEMONIC, `https://ropsten.infura.io/${process.env.INFURA_KEY}`),
      network_id: 3,
      gas: 6721975,
      gasPrice: 100000000000,
    },
    rinkeby: {
      provider: () => new HDWalletProvider(process.env.MNEMONIC, `https://rinkeby.infura.io/${process.env.INFURA_KEY}`),
      network_id: 4,
      gas: 6721975,
      gasPrice: 100000000000,
    },
  },
};
